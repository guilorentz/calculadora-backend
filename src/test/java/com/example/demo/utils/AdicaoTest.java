package com.example.demo.utils;



import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
public class AdicaoTest {

    @InjectMocks
    private Adicao adicao;

    @Test
    public void testeSomarValores() {
        double resultado = adicao.calcular(50, 25);
        assertEquals(75, resultado);
    }

}