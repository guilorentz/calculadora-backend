package com.example.demo.utils;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
public class SubtracaoTest {

    @InjectMocks
    private Subtracao subtracao;

    @Test
    public void testeSubtrairValores() {
        double resultado = subtracao.calcular(50, 25);
        assertEquals(25, resultado);
    }
}