package com.example.demo.controller;

import com.example.demo.service.impl.CalculadoraServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest //@WebMvcTest
public class CalculadoraControllerTest {

    @Autowired
    private CalculadoraController calculadoraController;

    @MockBean
    private CalculadoraServiceImpl calculadoraServiceImpl;
    @Autowired
    private MockMvc mockMvc;


    @Test
    public void testeCalculadoraControllerSemParametrosBadRequest() throws Exception {
        ResultActions response = mockMvc.perform(
                get("/calculadora"));
        response
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testeCalculadoraControllerSuccess() throws Exception {
        ResultActions response = mockMvc.perform(
                get("/calculadora")
                        .param("numeroUm", "20")
                        .param("numeroDois", "5")
                        .param("operador", "DIVISAO"));
        response
                .andExpect(status().isOk());
    }

    @Test
    public void testeCalculadoraControllerParametrosIncompletosSemNumeroUm() throws Exception {
        ResultActions response = mockMvc.perform(
                get("/calculadora")
                        .param("numeroDois", "5")
                        .param("operador", "DIVISAO"));
        response
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testeCalculadoraControllerParametrosIncompletosSemNumeroDois() throws Exception {
        ResultActions response = mockMvc.perform(
                get("/calculadora")
                        .param("numeroUm", "20")
                        .param("operador", "DIVISAO"));
        response
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testeCalculadoraControllerParametrosIncompletosSemOperador() throws Exception {
        ResultActions response = mockMvc.perform(
                get("/calculadora")
                        .param("numeroUm", "20")
                        .param("numeroDois", "5"));
        response
                .andExpect(status().isBadRequest());
    }
}