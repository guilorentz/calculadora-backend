package com.example.demo.enumerator;

import com.example.demo.utils.Calculo;
import com.example.demo.utils.Adicao;
import com.example.demo.utils.Divisao;
import com.example.demo.utils.Multiplicacao;
import com.example.demo.utils.Subtracao;

public enum Operacao  {

    ADICAO(new Adicao(), "+"),
    SUBTRACAO(new Subtracao(), "-"),
    MULTIPLICACAO(new Multiplicacao(), "*"),
    DIVISAO(new Divisao(), "/");

    private Calculo classeOperacao;
    private String descricao;

    Operacao(Calculo classeOperacao, String sinal) {
        this.classeOperacao = classeOperacao;
        this.descricao = sinal;
    }

    public Calculo getClasseOperacao() {
        return classeOperacao;
    }
    public String getDescricao() { return descricao; }

}