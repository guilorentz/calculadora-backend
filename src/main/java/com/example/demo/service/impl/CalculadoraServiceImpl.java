package com.example.demo.service.impl;

import com.example.demo.enumerator.Operacao;
import com.example.demo.exception.DivisaoPorZeroException;
import com.example.demo.service.CalculadoraService;
import org.springframework.stereotype.Service;

import java.sql.SQLOutput;

@Service
public class CalculadoraServiceImpl implements CalculadoraService {
    public double calcular (double numeroUm, double numeroDois, Operacao operador) throws Exception {
        verificarDivisaoPorZero(numeroDois, operador);

        return operador.getClasseOperacao().calcular(numeroUm, numeroDois);
    }

    private void verificarDivisaoPorZero (double numeroDois, Operacao operador) throws Exception {
        if (numeroDois == 0 && operador.getDescricao() == "/"){
            throw new DivisaoPorZeroException("Divisao por zero retorna um valor infinito!");
        }
    }
}