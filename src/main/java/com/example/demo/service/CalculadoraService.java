package com.example.demo.service;

import com.example.demo.enumerator.Operacao;

public interface CalculadoraService {

    double calcular (final double numeroUm, final double numeroDois, Operacao operador) throws Exception;
}
