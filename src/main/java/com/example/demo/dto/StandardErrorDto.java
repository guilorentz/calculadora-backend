package com.example.demo.dto;

import java.time.LocalDate;
import java.util.Date;

public class StandardErrorDto {

    private LocalDate timestamp;
    private Integer status;
    private String message;

    public StandardErrorDto(LocalDate timestamp, Integer status, String message) {
        this.timestamp = timestamp;
        this.status = status;
        this.message = message;
    }

    public LocalDate getTimestamp() {
        return timestamp;
    }
//
//    public void setTimestamp(LocalDate timestamp) {
//        this.timestamp = timestamp;
//    }
//
    public Integer getStatus() {
        return status;
    }
//
//    public void setStatus(Integer status) {
//        this.status = status;
//    }
//
    public String getMessage() {
        return message;
    }
//
//    public void setMessage(String message) {
//        this.message = message;
//    }
}
