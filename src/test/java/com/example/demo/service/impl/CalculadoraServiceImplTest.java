package com.example.demo.service.impl;

import com.example.demo.enumerator.Operacao;


import com.example.demo.exception.DivisaoPorZeroException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;

import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.*;

@RunWith (MockitoJUnitRunner.class)
public class CalculadoraServiceImplTest {

    @InjectMocks
    private CalculadoraServiceImpl calculadoraServiceImpl;

    @Test
    public void testeCalculadoraServiceAdicao() throws Exception{
        double resultado = calculadoraServiceImpl.calcular(25,5, Operacao.ADICAO);
        assertEquals(30, resultado);
    }
    @Test
    public void testeCalculadoraServiceSubtracao() throws Exception{
        double resultado = calculadoraServiceImpl.calcular(25,5, Operacao.SUBTRACAO);
        assertEquals(20, resultado);
    }
    @Test
    public void testeCalculadoraServiceMultiplicacao() throws Exception{
        double resultado = calculadoraServiceImpl.calcular(5,5, Operacao.MULTIPLICACAO);
        assertEquals(25, resultado);
    }
    @Test
    public void testeCalculadoraServiceDivisao() throws Exception{
        double resultado = calculadoraServiceImpl.calcular(25,5, Operacao.DIVISAO);
        assertEquals(5, resultado);
    }
    @Test(expected = DivisaoPorZeroException.class)
    public void testeCalculadoraServiceDivisaoExcecao() throws Exception{
        double resultado = calculadoraServiceImpl.calcular(25,0, Operacao.DIVISAO);
    }

}