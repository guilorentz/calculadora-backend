package com.example.demo.utils;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
public class MultiplicacaoTest {

    @InjectMocks
    private Multiplicacao multiplicacao;

    @Test
    public void testeMultiplicarValores(){
        double resultado = multiplicacao.calcular(8, 8);
        assertEquals(64, resultado);

    }

}