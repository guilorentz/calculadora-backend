package com.example.demo.utils;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
public class DivisaoTest {

    @InjectMocks
    private Divisao divisao;

    @Test
    public void testeDividirValores() {
        double resultado = divisao.calcular(10, 2);
        assertEquals(5, resultado);
    }

}