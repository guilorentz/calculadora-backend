package com.example.demo.controller;

import com.example.demo.dto.StandardErrorDto;
import com.example.demo.exception.DivisaoPorZeroException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDate;

@ControllerAdvice
public class CalculadoraExceptionHandler {

    @ExceptionHandler(value = {DivisaoPorZeroException.class})
    public ResponseEntity<StandardErrorDto> handleDivisaoPorZero(DivisaoPorZeroException exception) {
        StandardErrorDto erro = new StandardErrorDto(LocalDate.now(), HttpStatus.BAD_REQUEST.value(), exception.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(erro);
    }

    @ExceptionHandler(value = {IllegalArgumentException.class})
    public ResponseEntity<StandardErrorDto> handleIllegalArgument(IllegalArgumentException exception) {
        StandardErrorDto erro = new StandardErrorDto(LocalDate.now(), HttpStatus.BAD_REQUEST.value(), exception.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(erro);
    }

}
