package com.example.demo.utils;

public interface Calculo {
    public double calcular(double numeroUm, double numeroDois);

}
