package com.example.demo.utils;

public class Subtracao implements Calculo {

    @Override
    public double calcular(double numeroUm, double numeroDois) {
        return numeroUm - numeroDois;
    }
}
