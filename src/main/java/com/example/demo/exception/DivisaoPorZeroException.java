package com.example.demo.exception;

public class DivisaoPorZeroException extends Exception{

    public DivisaoPorZeroException(String mensagem){

        super (mensagem);
    }

}
