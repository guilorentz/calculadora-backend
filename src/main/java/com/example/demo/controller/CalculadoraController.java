package com.example.demo.controller;

import com.example.demo.service.impl.CalculadoraServiceImpl;
import com.example.demo.enumerator.Operacao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/calculadora")
public class CalculadoraController {

    @Autowired
    private CalculadoraServiceImpl calculadoraServiceImpl;

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping
    public double calculo(@RequestParam(value = "numeroUm", required = true) double numeroUm,
                          @RequestParam(value = "numeroDois", required = true) double numeroDois,
                          @RequestParam(value = "operador", required = true) Operacao operador) throws Exception {
        return calculadoraServiceImpl.calcular(numeroUm, numeroDois, operador);
    }
}