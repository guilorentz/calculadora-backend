package com.example.demo.utils;

public class Adicao implements Calculo {

    @Override
    public double calcular(double numeroUm, double numeroDois) {
        return numeroUm + numeroDois;
    }
}
